package test;

import org.junit.Test;
import genaretor.GenerateFileAndCheckData;

import java.io.IOException;

public class checkSortNumbersInFile {

    private GenerateFileAndCheckData generateFileAndCheckData = new GenerateFileAndCheckData();

    @Test
    public void testCheckSortNumbersInFile () throws IOException {
        generateFileAndCheckData
                .generateData()
                .saveDateInFile()
                .readFileAndSaveInList()
                .sortMinMax()
                .sortMin();

    }

}
