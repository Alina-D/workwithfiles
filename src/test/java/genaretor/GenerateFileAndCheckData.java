package genaretor;

import java.io.*;
import java.util.*;

import static java.lang.String.format;

public class GenerateFileAndCheckData {

    private static ArrayList<Integer> listForNumberGeneration = new ArrayList<>();
    private static ArrayList<Integer> listWithNumbersFromFile = new ArrayList<>();
    private static File file = new File("src/test/resources/fileWithNumbers.txt");


    /**
     * ��������� ���������� �������� �� 1 �� 10 ��������
     * @return this
     */
    public GenerateFileAndCheckData generateData () {
        Collections.addAll(listForNumberGeneration, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        Collections.shuffle(listForNumberGeneration);
        listForNumberGeneration.forEach(System.out::println);
        return this;
    }

    /**
     * �������� ������ ����� � ������ � ���� ������ ������
     * @return this
     */
    public GenerateFileAndCheckData saveDateInFile() throws IOException {
        Writer writer = new FileWriter(file);
        for(Integer number: listForNumberGeneration){
            if(listForNumberGeneration.indexOf(number) == listForNumberGeneration.size() - 1)   {
                writer.write(format("%s", number));
                break;
            }
            writer.write(format("%s,", number));
        }
        writer.close();
        return this;
    }

    /**
     * ��������� ������ ����� � �������� � ������
     * @return this
     */
    public GenerateFileAndCheckData readFileAndSaveInList() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String lineWithListNumber = reader.readLine();
        String[] parseLine = lineWithListNumber.split(",");
        for (String number: parseLine) {
            listWithNumbersFromFile.add(Integer.parseInt(number));
        }
        return this;
    }


    /**
     * ���������� �� �������� � ��������
     * @return this
     */
    public GenerateFileAndCheckData sortMinMax() {
        Collections.sort(listWithNumbersFromFile);
        listWithNumbersFromFile.forEach(System.out::println);
        return this;
    }

    /**
     * ���������� �� �������� � ��������
     * @return this
     */
    public GenerateFileAndCheckData sortMin() {
        Collections.reverse(listWithNumbersFromFile);
        listWithNumbersFromFile.forEach(System.out::println);
        return this;
    }
}
